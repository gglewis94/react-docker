# First stage named "builder" to run yarn and build my app
FROM node:18-buster-slim AS builder
# Set working directory
WORKDIR /app
# Copy all files from current directory to working dir in image
COPY . .
# install node modules and build assets
RUN yarn install && yarn build

# Second stage name "deploy" to retreive from the builder stage, only the necessary files
# We ommit the big "node modules" and keep copy only the assets we build in the prev stage
FROM nginx:1.20.0 as deploy
# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html
# Remove default nginx static assets
RUN rm -rf ./*
# Copy static assets from builder stage
COPY --from=builder /app/build .
# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]
